#!/usr/bin/python3
# -*- coding:utf-8 -*-

import requests
import time
import json

def day_list(j):
    data_init = requests.get('https://proxy.finance.qq.com/ifzqgtimg/appstock/app/newfqkline/get?param=%s,day,,,20,qfq'%j,timeout=(10,5)).text     # 前复权
    data_init = json.loads(data_init)
    try:
        data_list = data_init['data'][j]['qfqday']
    except:
        data_list = data_init['data'][j]['day']
    data_list = [float(i[2]) for i in data_list]
    return data_list

def code_num(codes,zhishu):
    sz399107 = zhishu
    code = day_list(codes)
    num = 3
    s_3 = (code[-1] - code[-num-1])/code[-num-1] - (sz399107[-1] - sz399107[-num-1])/sz399107[-num-1]
    num = 2
    s_2 = (code[-1] - code[-num-1])/code[-num-1] - (sz399107[-1] - sz399107[-num-1])/sz399107[-num-1]
    return s_2*100,s_3*100


def pianlizhi(code,zhishu):   
    c = code[0]
    p = code_num(c,zhishu)
    print(time.strftime("%H:%M:%S", time.localtime()))
    print(code[1],'\n2天偏离值:',p[0],'\n3天偏离值:',p[1])
    print('##############################################################')


while True:
    sz399107 = day_list('sz399107')
    # pianlizhi(['sz000957','中通客车'],sz399107)
    # pianlizhi(['sz000631','顺发恒业'],sz399107)
    pianlizhi(['sz002911','佛燃能源'],sz399107)
    xtime = int(time.strftime("%H%M%S", time.localtime()))
    if xtime < 93000:
        timestamp = int(time.time())
        timeArray_day = time.strptime(time.strftime("%Y-%m-%d", time.localtime()) + ' 9:30:00', "%Y-%m-%d %H:%M:%S")
        timestamp_day = int(time.mktime(timeArray_day))
        print('请等待9点30')
        time.sleep(timestamp_day - timestamp)
    elif 113000 < xtime < 130000:
        timestamp = int(time.time())
        timeArray_day = time.strptime(time.strftime("%Y-%m-%d", time.localtime()) + ' 13:00:00', "%Y-%m-%d %H:%M:%S")
        timestamp_day = int(time.mktime(timeArray_day))
        print('请等待13点00')
        time.sleep(timestamp_day - timestamp)
    elif 150000 < xtime:
        break
    time.sleep(3)