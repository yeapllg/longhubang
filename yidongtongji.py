#!/usr/bin/python3
# -*- coding:utf-8 -*-

import requests
import time
import json
import pandas as pd
import datetime
import re
import tushare as ts

def day_data():
    day_n = '近10个交易日的日期列表，请自行获取'
    return day_n

def code_yd(code,day_n):
    headers = {
        'Host':'proxy.finance.qq.com',
        'Connection':'keep-alive',
        'Accept':'*/*',
        'User-Agent':'QQStock/10.2.0 (iPhone; iOS 15.6; Scale/3.00)',
        'Accept-Language':'zh-Hans-CN;q=1',
        'Referer':'http://zixuanguapp.finance.qq.com'
    }

    data = requests.get('https://proxy.finance.qq.com/ifzqgtimg/appstock/news/noticeList/searchByType?symbol=%s&page=1&n=50'%code,headers=headers,timeout=(10,5))
    data = json.loads(data.text)
    data = data['data']['data']
    # data = [[i['title'],i['time']] for i in data if '异常波动' in i['title']]
    data_day = [i['time'][:10] for i in data if '异常波动' in i['title']]
    data_day = [re.sub('-','',i) for i in data_day]
    # print(data_day)
    s = [i for i in day_n if i in data_day]
    return len(s)

def main():
    ts.set_token('你的token，请自行去tusare申请')
    pro = ts.pro_api()
    data = pro.stock_basic(exchange='', list_status='L', fields='ts_code,name')
    list_codes = []
    dict_codes = {}
    for i in [i for i in data.name.values if 'ST' not in i]:
        dict_codes[data[data.name == i].ts_code.values[0]] = i
        list_codes.append(data[data.name == i].ts_code.values[0])
    list_codes = [i[-2:].lower()+i[:-3] for i in list_codes]
    list_codes = [i for i in list_codes if i[2] == '0']

    for i in list(dict_codes.keys()):
        if i[-2:].lower()+i[:-3] in list_codes:
            dict_codes[i[-2:].lower()+i[:-3]] = dict_codes[i]
            del dict_codes[i]
        else:
            del dict_codes[i]


    day_n = day_data()
    gupiaos = []
    jiekou_dict = {}
    for code in list(dict_codes.keys()):
        n = code_yd(code,day_n)
        print('%s---%s---%s'%(code,dict_codes[code],n))
        if n >= 2:
            gupiaos.append('%s---%s---%s'%(code,dict_codes[code],n))
            jiekou_dict[code] = n
        time.sleep(0.5)
    if len(gupiaos) > 0:
        print(jiekou_dict)

if __name__ == "__main__":
    # main()
    while True:
        now_1 = str(datetime.datetime.now())[:10]
        if 230000 > int(time.strftime("%H%M%S", time.localtime())):
            timestamp = int(time.time())
            timeArray_day = time.strptime(time.strftime("%Y-%m-%d", time.localtime()) + ' 23:00:00', "%Y-%m-%d %H:%M:%S")
            timestamp_day = int(time.mktime(timeArray_day))
            time.sleep(timestamp_day - timestamp)
            main()
            if now_1 == str(datetime.datetime.now())[:10]:
                timestamp = int(time.time())
                timeArray_day = time.strptime(time.strftime("%Y-%m-%d", time.localtime()) + ' 23:59:59', "%Y-%m-%d %H:%M:%S")
                timestamp_day = int(time.mktime(timeArray_day))
                time.sleep(timestamp_day - timestamp + 10)
        else:
            main()
            if now_1 == str(datetime.datetime.now())[:10]:
                timestamp = int(time.time())
                timeArray_day = time.strptime(time.strftime("%Y-%m-%d", time.localtime()) + ' 23:59:59', "%Y-%m-%d %H:%M:%S")
                timestamp_day = int(time.mktime(timeArray_day))
                time.sleep(timestamp_day - timestamp + 10)