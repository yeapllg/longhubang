# 北京炒家首版算法

def code_list_20(l):
    close_list = [float(i[2]) for i in l]
    jun = sum(close_list) / len(close_list)
    zuigao = (max(close_list) - jun)*100 / jun
    zuidi = (jun - min(close_list))*100 / jun
    mn = (max(close_list) - min(close_list))*100 / min(close_list)
    if zuigao < 10 and zuidi < 10 and mn < 10:
        return lines(close_list)
    else:
        return 1

def lines(x):
    # x = [float(i[2]) for i in x]
    slope = np.polyfit([i for i in range(len(x))], x, 1)    #直线
    # print(slope)
    if slope[0] < 0.00000001:
        # y=slope[1]
        A = 0
        B = 1
        C = round(-slope[1],4)
    else:
        # y = slope[0]x
        A = round(-slope[0],4)
        B = 1
        C = round(-slope[1],4)
    # Ax + By + C = 0  --->  y = (-Ax -C)/B
    lists = [[i,x[i]] for i in range(len(x))]
    distance = [np.abs(A * i[0] + B * i[1] + C) / (np.sqrt(A**2 + B**2)) for i in lists]
    # return sum(distance)/len(distance),slope[0]
    if abs(slope[0])*10 < max(distance):
        return 1
    else:
        return 0

list_codes = [] # 这里是所有股票代码的列表，注意，这里的票需要排除上个交易日涨停的股票
print(list_codes)


list_res = []
list_paichu = []
for j in list_codes:
    time.sleep(0.5)
    print(j,'开始执行!!!')
    data = requests.get('https://proxy.finance.qq.com/ifzqgtimg/appstock/app/newfqkline/get?param=%s,day,,,110,qfq'%j,timeout=(10,5)).text
    data = json.loads(data)
    try:
        data_list = data['data'][j]['qfqday']
    except:
        data_list = data['data'][j]['day']
    if len(data_list) < 100:
        continue
    csd = code_list_20(data_list[-20:])
    if csd == 1:
        list_paichu.append(j)
    else:
        print(j,csd)
        list_res.append(j)
print(list_res)
print('%s###########################################%s'%('\n','\n'))
print(list_paichu)

# list_res 就是备选股票，大概每天会有100多只，接下来就是自己写一个循环，每秒钟循环一次，当股票涨停的时候，或者即将涨停的时候发出提示就行，至于这只票是不是当前热点就得人工判断了，还有，需要排除当前价格小于近40个交易日的最大收盘价！