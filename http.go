package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

var qingxu_value = 0 // 情绪全局变量，1为好，0为不好

type Tableths struct {
	Value int
	Key   string
}

type ResponseData struct {
	Status_code int
	Status_msg  string
	Data        struct {
		Suspend          int
		Last_update_time string
		Limit_down       int
		Limit_up         int
		Flat             int
		Up               int
		Down             int
		Table            []Tableths
	}
}

func main() {
	sic := []int{}
	client := &http.Client{
		Timeout: 5 * time.Second, // 设置超时时间
	}
	reqURL := "https://dq.10jqka.com.cn/fuyao/up_down_distribution/distribution/v2/realtime?"
	// 创建请求头
	reqHeaders := map[string]string{
		"Host":            "dq.10jqka.com.cn",
		"Sec-Fetch-Site":  "same-site",
		"sw8":             "1-MjQ1MDUxMWYtODY3Zi00YTU3LWI1M2UtMDE2YzVjNmQ1MjYz-NmU4ZjEwOTctZjMyYi00OTZiLTkwNTEtNTg4OWI3NDc4MDJi-0-dW5kZWZpbmVkPGJyb3dzZXI+-dmVyc2lvbl8wMQ==-UFJPR1JBTS1sb2dpY2FsT2ZIb3RTdG9jaw==-ZHEuMTBqcWthLmNvbS5jbg==",
		"Accept-Language": "zh-CN,zh-Hans;q=0.9",
		"Sec-Fetch-Mode":  "cors",
		"Accept":          "application/json, text/plain, */*",
		"Origin":          "https://eq.10jqka.com.cn",
		"User-Agent":      "Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 hxFont/normal getHXAPPAdaptOldSetting/0 Language/zh-Hans getHXAPPAccessibilityMode/0 hxnoimage/0 getHXAPPFontSetting/small VASdkVersion/1.1.2 VoiceAssistantVer/0 hxtheme/0 IHexin/11.50.71 (Royal Flush) userid/XXXXXXX innerversion/I037.08.521 build/11.50.74 surveyVer/0 isVip/1",
		"Connection":      "keep-alive",
		"Referer":         "https://eq.10jqka.com.cn/",
		"Sec-Fetch-Dest":  "empty",
	}

	// 初始化请求
	req, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		fmt.Println("Error creating request: %s", err)
		time.Sleep(5 * time.Second)
	}

	// 设置请求头
	for k, v := range reqHeaders {
		req.Header.Set(k, v)
	}

	// 发起请求
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request: %s", err)
		time.Sleep(5 * time.Second)
	}
	// defer resp.Body.Close()
	// 处理响应
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body: %s", err)
		time.Sleep(5 * time.Second)

	}
	// fmt.Println(string(body))
	var responseData ResponseData
	err = json.Unmarshal(body, &responseData)
	if err != nil {
		fmt.Println("Error unmarshalling JSON: %s", err)
		time.Sleep(5 * time.Second)
	}
	// fmt.Println(responseData.Data.Up)
	// fmt.Println(responseData.Data.Down)
	// fmt.Println(responseData.Data.Flat)
	// fmt.Println(responseData.Data.Flat + responseData.Data.Up + responseData.Data.Down)
	zuidiup := (responseData.Data.Flat + responseData.Data.Up + responseData.Data.Down) * 2 / 10
	zuizuidiup := (responseData.Data.Flat + responseData.Data.Up + responseData.Data.Down) * 1 / 10
	fmt.Println(zuizuidiup)
	fmt.Println(zuidiup)
	sic = append(sic, responseData.Data.Up)
	resp.Body.Close()
	if len(sic) >= 40 {
		sic_10 := sic[len(sic)-40:]
		num := 0
		for _, v := range sic_10 {
			num = num + v
		}
		// qingxu_value 情绪全局变量，1为好，0为不好,-1为极差
		if num/len(sic_10) > zuidiup {
			qingxu_value = 1
		} else if num/len(sic_10) <= zuizuidiup {
			qingxu_value = -1
		} else {
			qingxu_value = 0
		}
	} else {
		sic_10 := sic
		num := 0
		for _, v := range sic_10 {
			num = num + v
		}
		if num/len(sic_10) > zuidiup {
			qingxu_value = 1
		} else if num/len(sic_10) <= zuizuidiup {
			qingxu_value = -1
		} else {
			qingxu_value = 0
		}
	}
	fmt.Println(qingxu_value)
}
